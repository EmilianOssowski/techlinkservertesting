package main;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

import loggingTests.Failed_Logging;
import loggingTests.Logging;

import loggingTests.Null_Logging;
import overlapsTests.Main_Overlaps;
import overlapsTests.Start_Overlap;
import services.CounterService;
import services.DriverService;
import services.LoggerService;
import services.TestModel;
import services.TestService;

public class Main {
	static TestService testService;
	static DriverService driverService;
	
	public static void main(String[] args) throws Exception{
		System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver_win32\\chromedriver.exe");
		System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver-v0.21.0-win64\\geckodriver.exe");

		LoggerService.LoggerSetup();
		LoggerService.addInfo("Initiazlization tests, Date: " + LocalTime.now() +"  "+ LocalDate.now());



		testService = new TestService(new Failed_Logging());
		testService = new TestService(new Null_Logging());
		testService = new TestService(new Logging());
		DriverService.setUp();
		testService = new TestService(new Main_Overlaps(DriverService.driver));
		testService = new TestService(new Start_Overlap(DriverService.driver));
		DriverService.tearDown();


		LoggerService.addInfo(CounterService.getResult());
	}

}
