package loggingTests;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;
import static org.junit.Assert.assertFalse;

import java.util.concurrent.TimeUnit;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import services.DriverService;
import services.LoggerService;
import services.LoginService;
import services.TestModel;

public class Null_Logging extends TestModel{
		private WebDriver driver;
		private String baseUrl;
		private boolean acceptNextAlert = true;
		private StringBuffer verificationErrors = new StringBuffer();
		LoginService loginservice;

	  @Before
	  @Override
	  public void setUp() throws Exception {
		  
		  //driver = new FirefoxDriver();
		  System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver_win32\\chromedriver.exe");
		  System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver-v0.21.0-win64\\geckodriver.exe");
		  baseUrl = "http://192.168.1.182";
		  driver = new ChromeDriver();
		  driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
		  loginservice = new LoginService();
	  }
	  @Test
	  @Override
	  public void test() throws Exception{
		  driver.get(baseUrl);
		  driver.findElement(By.id("user_login")).click();
		  driver.findElement(By.id("user_login")).clear();
		  driver.findElement(By.id("user_login")).sendKeys("");
		  driver.findElement(By.id("pass")).clear();
		  driver.findElement(By.id("pass")).sendKeys("");
		  driver.findElement(By.name("commit")).click();
		  assertTrue(isElementPresent(By.xpath("//*[contains(text(),'ZALOGUJ SIĘ DO PLATFORMY INTEGRACYJNEJ')]")));
		  assertTrue(isElementPresent(By.xpath("//*[contains(text(),'Logowanie nie powiodło się')]")));
	  }
	  @After
	  @Override
	  public void tearDown() throws Exception {
	    driver.quit();
	    
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	      
	    }
	    
	  }
	  
	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}


