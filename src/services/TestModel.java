package services;

public abstract class TestModel{
	public abstract void setUp() throws Exception; 
	public abstract void test() throws Exception;
	public abstract void tearDown() throws Exception;
	
}