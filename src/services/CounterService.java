package services;

public class CounterService {
	public static int all = 0;
	public static int passed = 0;
	public static int failed = 0;

	
	public static String getResult() {
		StringBuilder result = new StringBuilder();
		result.append("Test results:\n");
		result.append("All tests:    " + all+"\n");
		result.append("-Passed tests: " + passed +", " + ((float)passed/all)*100+"%\n");
		result.append("-Failed tests: " + failed +", " + ((float)failed/all)*100+"%\n");
		result.append(progressPercentage((int)(((float)passed/all)*100)));
		
		return result.toString();
	}

	private static String progressPercentage(int remain) {
	    
	    
	    char[] bar = new char[76];
	    bar[0] = '[';
	    for (int i = 1; i < 75; i++) {
	        bar[i] = ' ';
	    }
	    bar[75] = ']';
	    for (int i = 1; i <= remain*74/100; i++) {
	        bar[i] = '•';
	    }
	    
	    StringBuilder bare = new StringBuilder();
	    bare.append("Automatic Test Result: \n");
	    bare.append(new String(bar));
	    bare.append(" " + remain + "%\n");
	    return bare.toString();
	}
		
	
}
