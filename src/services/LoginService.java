package services;



import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginService {
	
	//metoda logująca na podanego użytkwnika
	public void login(String user_login, String pass, WebDriver driver) {
		 
		    DriverService.driver.findElement(By.id("user_login")).click();
		    DriverService.driver.findElement(By.id("user_login")).clear();
		    DriverService.driver.findElement(By.id("user_login")).sendKeys(user_login);
		    DriverService.driver.findElement(By.id("pass")).clear();
		    DriverService.driver.findElement(By.id("pass")).sendKeys(pass);
		    DriverService.driver.findElement(By.name("commit")).click();
		   
	}
	
	public void logout(WebDriver driver) {
		DriverService.driver.findElement(By.xpath("//*[@value= 'WYLOGUJ']")).click();
	}

}
