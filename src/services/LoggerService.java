package services;

import java.io.IOException;
import java.util.Date;
import java.util.logging.ConsoleHandler;
import java.util.logging.FileHandler;
import java.util.logging.LogRecord;
import java.util.logging.Logger;
import java.util.logging.SimpleFormatter;

public class LoggerService {
	public static Logger logger = Logger.getLogger("com.javacodegeeks.snippets.core");

	
	public static void LoggerSetup() throws Exception{
		boolean append = true;
		FileHandler handler = new FileHandler("TestLog.txt" , append);
		ConsoleHandler ch = new ConsoleHandler();		
        
       
        handler.setFormatter(new SimpleFormatter( ) {
        	private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

            @Override
            public synchronized String format(LogRecord lr) {
                return String.format(format,
                        new Date(lr.getMillis()),
                        lr.getLevel().getLocalizedName(),
                        lr.getMessage()
                );
            }	
        	
        });  
        
        ch.setFormatter(new SimpleFormatter( ) {
        	private static final String format = "[%1$tF %1$tT] [%2$-7s] %3$s %n";

            @Override
            public synchronized String format(LogRecord lr) {
                return String.format(format,
                        new Date(lr.getMillis()),
                        lr.getLevel().getLocalizedName(),
                        lr.getMessage()
                );
            }	
        	
        });  
        logger.addHandler(handler);
        //logger.addHandler(ch);
        logger.info("Tests started");
	}
	
	
    	
		
	
	public static void addInfo(String log) {
		logger.info(log);
	}
	public static void addWarning(String log) {
		logger.warning(log);
	}
	public static void addSevere(String log) {
		logger.severe(log);
	}

}
