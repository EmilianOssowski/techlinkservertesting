package services;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static org.junit.Assert.fail;


public class DriverService {

    public static WebDriver driver = new ChromeDriver();
    public static String baseUrl = "http://192.168.1.182";
    private static StringBuffer verificationErrors = new StringBuffer();

    DriverService() {
        System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver_win32\\chromedriver.exe");
        System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver-v0.21.0-win64\\geckodriver.exe");
        //driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    }

    public static void setUp() {


    }

    public static void tearDown() {
        driver.quit();
        String verificationErrorString = verificationErrors.toString();
        if (!"".equals(verificationErrorString)) {
            fail(verificationErrorString);
        }
    }

    public static WebDriver getDriver() {
        return driver;
    }

    public String getBaseUrl() {
        return baseUrl;
    }

}

