package services;

import services.CounterService;

public class TestService{
	public TestService(TestModel test) throws Exception
	{
		try {
			CounterService.all = CounterService.all + 1;
			//LoggerService.addInfo("Started " + test.getClass().toString());
			test.setUp();
			test.test();
			test.tearDown();
			//LoggerService.addInfo("Ended " + test.getClass().toString());
			CounterService.passed = CounterService.passed + 1;
			LoggerService.addInfo("SUCCESS " + test.getClass().toString());
		}
		catch(AssertionError e) {
			LoggerService.addWarning( "FAILED" +" Assertion error in "+ test.getClass().toString());
			test.tearDown();
			CounterService.failed = CounterService.failed +1;
		}
		catch(Error e) {
			LoggerService.addSevere("ERROR:\n in " + test.getClass().toString() +" : " + e.toString());
			test.tearDown();
			CounterService.failed = CounterService.failed +1;
		}
		
	}
	

}


