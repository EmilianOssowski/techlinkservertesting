package overlapsTests;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import services.DriverService;
import services.LoginService;
import services.TestModel;

import java.util.concurrent.TimeUnit;

public class Main_Overlaps extends TestModel {


    private WebDriver driver ;
    private String baseUrl;
    private boolean acceptNextAlert = true;
    private StringBuffer verificationErrors = new StringBuffer();
    LoginService loginservice;

    public Main_Overlaps(WebDriver driver){
        this.driver = driver;
        loginservice = new LoginService();
    }

    @Before
    @Override
    public void setUp() throws Exception {
        System.setProperty("webdriver.chrome.driver", ".\\drivers\\chromedriver_win32\\chromedriver.exe");
        // System.setProperty("webdriver.gecko.driver", ".\\drivers\\geckodriver-v0.21.0-win64\\geckodriver.exe");
        //baseUrl = "http://192.168.1.182";
        //driver = new ChromeDriver();
        //driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        //loginservice = new LoginService();
    }

    @Test
    @Override
    public void test() throws Exception {
        driver.get(DriverService.baseUrl);
        loginservice.login("admin", "123456", DriverService.driver);
        driver.findElement(By.className("buttonGoTo")).click();

    }

    @After
    @Override
    public void tearDown() throws Exception {

    }
}
